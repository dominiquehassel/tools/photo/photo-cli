.PHONY: help run build install binary

help:
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help

GOBIN=$(GOPATH)/bin
GOCMD=go
GOBUILD=$(GOCMD) build
BINARY_NAME=photo

ifneq ("$(wildcard VERSION)","")
VERSION=$(shell cat VERSION)
else
VERSION=$(shell git rev-list -1 HEAD)
endif

# If the first argument is "run"...
ifeq (run,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

run:
	$(GOCMD) run main.go $(RUN_ARGS)

all: test build ## run all build and test targets

build:
	$(GOBUILD) -o $(BINARY_NAME) -ldflags "-X main.version=$(VERSION)"

install: ## build locally and copy bin into $(GOROOT)/bin
	$(GOCLEAN)
	$(GOBUILD) -o $(GOBIN)/$(BINARY_NAME) -ldflags "-X main.version=$(VERSION)"

build-all:
	$(GOCLEAN)
	rm -rf build
	@GOOS=darwin GOARCH=amd64 GOBIN=$(GOBIN) $(GOBUILD) \
			-tags release \
			-ldflags '-X main.version=$(VERSION)' \
			-o build/$(BINARY_NAME)-darwin-amd64
	@GOOS=linux GOARCH=amd64 GOBIN=$(GOBIN) $(GOBUILD) \
			-tags release \
			-ldflags '-X main.version=$(VERSION)' \
			-o build/$(BINARY_NAME)-linux-amd64
	@GOOS=windows GOARCH=amd64 GOBIN=$(GOBIN) $(GOBUILD) \
			-tags release \
			-ldflags '-X main.version=$(VERSION)' \
			-o build/$(BINARY_NAME)-windows-amd64.exe
	tar -czvf build/$(BINARY_NAME)-${VERSION}-darwin-amd64.tar.gz build/$(BINARY_NAME)-darwin-amd64
	tar -czvf build/$(BINARY_NAME)-${VERSION}-linux-amd64.tar.gz build/$(BINARY_NAME)-linux-amd64
	tar -czvf build/$(BINARY_NAME)-${VERSION}-win-amd64.tar.gz build/$(BINARY_NAME)-windows-amd64.exe