package action

import (
	"fmt"
	"path/filepath"
	"strconv"
	"strings"
	"sync"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/dominiquehassel/tools/photo/cli/pkg/common"
	fh "gitlab.com/dominiquehassel/tools/photo/cli/pkg/file"
)

type Copy struct {
	next            common.Processing
	defaultRepoPath string
	fh              *fh.FileHandle
	sort            Sort
}

type Sort interface {
	getTarget(file *common.File) string
}

func NewCopyMaster(p common.Processing, defaultRepoPath string, sort Sort) *Copy {
	c := &Copy{next: p, defaultRepoPath: defaultRepoPath, fh: fh.NewFileHandle(), sort: sort}
	return c
}

func (c *Copy) CreateWorkers(wg *sync.WaitGroup, in chan *common.File, logger *logrus.Logger) {
	out := make(chan *common.File)
	for w := 0; w < viper.GetInt("worker.count"); w++ {
		logger.WithFields(logrus.Fields{"method": "CreateWorkers", "type": "copy"}).Debugf("setting up worker %d", w)
		go c.copyPhoto(fmt.Sprintf("Copy_Worker_%d", w), wg, in, out, logger)
	}
	c.next.CreateWorkers(wg, out, logger)
}

func (c *Copy) copyPhoto(w string, wg *sync.WaitGroup, in chan *common.File, out chan *common.File, logger *logrus.Logger) {
	l := logger.WithFields(logrus.Fields{"type": "copy", "method": "copy"})
	for f := range in {
		l.Debugf("%s: %s \n", w, f.Path)
		fileTarget := c.sort.getTarget(f)
		fileTarget = filepath.Join(c.defaultRepoPath, fileTarget)
		c.fh.CreateIfNotExists(fileTarget)
		target := filepath.Join(fileTarget, f.Info.Name())
		if ok, _ := fh.Exists(target); ok {
			l.Warningf("File already exists: %s", target)
			// TODO: handle duplicates
		}
		if e := fh.CopyToTarget(f.Path, target); e != nil {
			l.Errorf("Could not copy file: %s", e.Error())
		} else {
			if c.next != nil {
				out <- &common.File{Path: target, Info: f.Info}
				wg.Add(1)
			}
		}
		wg.Done()
	}
}

type NoSort struct {
	root string
}

func NewNoSort(root string) *NoSort {
	r, err := filepath.Abs(root)
	if err != nil {
		r = root
	}
	return &NoSort{root: r}
}

func (n *NoSort) getTarget(file *common.File) string {
	return strings.ReplaceAll(file.Path, n.root, "")
}

type DateSort struct {
}

func (d *DateSort) getTarget(file *common.File) string {
	year := file.Info.ModTime().Year()
	month := file.Info.ModTime().Month().String()
	return filepath.Join(strconv.Itoa(year), month)
}
