package action

import (
	"fmt"
	"sync"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/dominiquehassel/tools/photo/cli/pkg/common"
)

type Import struct {
}

func NewImportMaster() *Import {
	return &Import{}
}
func (c *Import) CreateWorkers(wg *sync.WaitGroup, in chan *common.File, logger *logrus.Logger) {
	for w := 0; w < viper.GetInt("worker.count"); w++ {
		logger.WithFields(logrus.Fields{"method": "CreateWorkers", "type": "import"}).Debugf("setting up worker %d", w)
		go c.importPhoto(fmt.Sprintf("Import_Worker_%d", w), wg, in, logger)
	}
}

func (c *Import) importPhoto(w string, wg *sync.WaitGroup, in chan *common.File, logger *logrus.Logger) {
	l := logger.WithFields(logrus.Fields{"type": "import", "method": "import"})
	for f := range in {
		l.Debugf("%s: %s \n", w, f.Path)
		wg.Done()
	}
}
