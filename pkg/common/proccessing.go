package common

import (
	"os"
	"sync"

	"github.com/sirupsen/logrus"
)

type File struct {
	Path string
	Info os.FileInfo
}

type Processing interface {
	CreateWorkers(wg *sync.WaitGroup, io chan *File, logger *logrus.Logger)
}
