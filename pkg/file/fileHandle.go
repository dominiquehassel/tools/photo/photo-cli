package file

import (
	"io"
	"os"
	"sync"
)

type FileHandle struct {
	mutex sync.Mutex
}

func NewFileHandle() *FileHandle {
	return &FileHandle{mutex: sync.Mutex{}}
}

func (fh *FileHandle) CreateIfNotExists(path string) {
	fh.mutex.Lock()

	if ok, _ := Exists(path); !ok {
		os.MkdirAll(path, os.ModePerm)
	}
	fh.mutex.Unlock()
}

// checks whether the file/directory exists.
// In case of an error the result will be true along with the error.
func Exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

func MoveToTarget(source string, target string) error {
	err := os.Rename(source, target)
	if err != nil {
		return err
	}
	return nil
}

func CopyToTarget(source string, target string) error {
	from, err := os.Open(source)
	if err != nil {
		return err
	}
	defer from.Close()

	to, err := os.OpenFile(target, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	defer to.Close()

	_, err = io.Copy(to, from)
	if err != nil {
		return err
	}
	return nil
}
