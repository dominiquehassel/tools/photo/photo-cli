package file

import (
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/sirupsen/logrus"
	"gitlab.com/dominiquehassel/tools/photo/cli/pkg/common"
)

type Crawler struct {
	io      chan *common.File
	logger  *logrus.Entry
	wg      *sync.WaitGroup
	filters []string
}

func NewCrawler(p common.Processing, logger *logrus.Logger) *Crawler {
	c := &Crawler{
		io:      make(chan *common.File, 50),
		logger:  logger.WithField("type", "Importer"),
		wg:      &sync.WaitGroup{},
		filters: []string{".jpg", ".JPG", ".heic", ".HEIC"},
	}
	p.CreateWorkers(c.wg, c.io, logger)
	return c
}

func (c *Crawler) Run(pathToPhotos string) error {
	l := c.logger.WithField("method", "ImportPhotos")
	l.Debug("reading files")
	err := filepath.Walk(pathToPhotos, c.postFile)
	if err != nil {
		return err
	}
	close(c.io)
	l.Debug("wait for worker to finish")
	c.wg.Wait()
	l.Info("Finished processing files.")
	return nil
}

func (c *Crawler) postFile(path string, info os.FileInfo, err error) error {
	if info != nil && !info.IsDir() && c.filter(info.Name()) {
		if err != nil {
			return err
		}
		c.io <- &common.File{Path: path, Info: info}
		c.wg.Add(1)
	}
	return nil
}

func (c *Crawler) filter(name string) bool {
	for _, ext := range c.filters {
		if strings.HasSuffix(name, ext) {
			return true
		}
	}
	return false
}
