/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/dominiquehassel/tools/photo/cli/pkg/action"
	"gitlab.com/dominiquehassel/tools/photo/cli/pkg/common"
	fh "gitlab.com/dominiquehassel/tools/photo/cli/pkg/file"
)

var target string
var copy, sort bool

// importCmd represents the import command
var importCmd = &cobra.Command{
	Use:   "import",
	Short: "imports all photos found to the magazin",
	Long: `Runs through all directories within the given paht looking for photos to import.
	These photos will be copied to the default repository path ordered by the creation date of the photo.
	The original photos will not be deleted! 

	Example usage: 
	$ photo import <path-to-import-directory>`,
	Run: performImport,
}

func performImport(cmd *cobra.Command, args []string) {
	configLogger()
	logEntries := logger.WithFields(logrus.Fields{"cmd": "import"})
	if len(args) != 1 {
		logEntries.Fatalf("Expected only one argument but got %d", len(args))

	}
	pathToImport := args[0]
	l := logEntries.WithField("method", "performImport")
	l.Debugf("import photos from %s", pathToImport)
	l.Debug("created importer")
	var p common.Processing
	if copy {
		var sorting action.Sort
		if sort {
			sorting = &action.DateSort{}
		} else {
			sorting = action.NewNoSort("")
		}
		p = action.NewCopyMaster(action.NewImportMaster(), target, sorting)
	} else {
		p = action.NewImportMaster()
	}
	c := fh.NewCrawler(p, logger)
	if err := c.Run(pathToImport); err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.AddCommand(importCmd)

	importCmd.Flags().StringVarP(&target, "target", "t", "", "The target path to move the photos into")
	importCmd.Flags().BoolVarP(&copy, "copy", "c", false, "copies the files to the default repository path")
	importCmd.Flags().BoolVarP(&sort, "sort", "s", false, `Sorts the files copied by creation time. 
This option will only e going to work along with copying the files to the repository path.
	`)
}
